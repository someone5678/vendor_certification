# vendor_parasite_certification

* First install jq for json parsing.

```bash
sudo apt install jq
```

* You can use your own `pif.json` by put it under `configs` folder and run `setup-buildprops.sh`
* After that, add inherit on your vendor:

```makefile
$(call inherit-product-if-exists, vendor/parasite/certification/config.mk)
```

## License

All code in this repository is licensed under the Apache 2.0 License.

Copyright (C) 2023-2025 TheParasiteProject
