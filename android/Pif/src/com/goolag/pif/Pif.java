/*
 * Copyright (C) 2023-2024 The Evolution X Project
 * SPDX-License-Identifier: Apache-2.0
 */

package com.goolag.pif;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.IPackageDataObserver;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;

import com.android.internal.util.custom.CustomUtils;
import com.android.internal.util.custom.certification.Android.PiHookProperties;
import com.android.settingslib.widget.TopIntroPreference;

import com.google.gson.Gson;

import lineageos.preference.SystemPropertySwitchPreference;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class Pif extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {

    private static final String TAG = "Pif";

    // internal constants used in Handler
    private static final int OP_SUCCESSFUL = 1;
    private static final int OP_FAILED = 2;

    private ClearCacheObserver mClearCacheObserver;
    private PackageManager mPm;

    private TopIntroPreference mIntroPreference;
    private SystemPropertySwitchPreference mCertHook;
    private Preference mPifJsonImporter;
    private PreferenceCategory mInfo;
    private PreferenceCategory mAttestation;
    private HashMap<String, Preference> mInfoPreferences;
    private HashMap<String, Preference> mAttestationPreferences;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.main, rootKey);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIntroPreference = findPreference("device_intro");
        mIntroPreference.setTitle(Build.MANUFACTURER + " " + Build.MODEL);

        mCertHook = (SystemPropertySwitchPreference) findPreference("persist.sys.certhook.enable");
        mCertHook.setOnPreferenceChangeListener(this);

        mPifJsonImporter = (Preference) findPreference("pif_json_importer");
        mPifJsonImporter.setOnPreferenceClickListener(
                preference -> {
                    openFileSelector(10001);
                    return true;
                });

        mInfo = (PreferenceCategory) findPreference("info");
        mAttestation = (PreferenceCategory) findPreference("attestation");

        mInfoPreferences = new HashMap<>();
        mAttestationPreferences = new HashMap<>();

        updateSummary(true);
    }

    private void openFileSelector(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/json");
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK || data == null) return;
        Uri uri = data.getData();
        if (uri == null) return;
        if (requestCode == 10001) {
            loadPifJson(uri);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        final String key = preference.getKey();
        switch (key) {
            case "persist.sys.certhook.enable":
                restartApps();
                return true;
        }
        return false;
    }

    private void loadPifJson(Uri uri) {
        Log.d(TAG, "Loading PIF JSON from URI: " + uri.toString());
        try (InputStream inputStream = getActivity().getContentResolver().openInputStream(uri)) {
            if (inputStream == null) return;
            String json = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            if (json == null) return;
            Log.d(TAG, "PIF JSON data: " + json);
            HashMap<String, Object> map = new Gson().fromJson(json, HashMap.class);
            if (map == null || map.isEmpty()) return;
            putIfNotExists(map, "DEVICE_FOR_ATTESTATION", "DEVICE");
            putIfNotExists(map, "PRODUCT_FOR_ATTESTATION", "PRODUCT");
            putIfNotExists(map, "MODEL_FOR_ATTESTATION", "MODEL");
            putIfNotExists(map, "BRAND_FOR_ATTESTATION", "BRAND");
            putIfNotExists(map, "MANUFACTURER_FOR_ATTESTATION", "MANUFACTURER");
            putIfNotExists(map, "DEVICE_INITIAL_SDK_INT", "24");
            map.forEach((k, v) -> setProp(k, v));
        } catch (Exception e) {
            Log.e(TAG, "Error reading PIF JSON or setting properties", e);
        }
        restartApps();
        updateSummary(false);
    }

    private void putIfNotExists(HashMap<String, Object> map, String key, String src) {
        if (!map.keySet().contains(key)) {
            map.put(key, map.get(src));
        }
    }

    private void setProp(String key, Object value) {
        String targetKey = "";
        switch (key) {
            case "DEVICE":
            case "FINGERPRINT":
            case "PRODUCT":
            case "MODEL":
            case "BRAND":
            case "SECURITY_PATCH":
            case "MANUFACTURER":
            case "BOARD":
            case "HARDWARE":
            case "DEVICE_INITIAL_SDK_INT":
            case "RELEASE":
            case "ID":
            case "INCREMENTAL":
            case "TYPE":
            case "TAGS":
            // Additional props
            case "DEVICE_FOR_ATTESTATION":
            case "PRODUCT_FOR_ATTESTATION":
            case "MODEL_FOR_ATTESTATION":
            case "BRAND_FOR_ATTESTATION":
            case "MANUFACTURER_FOR_ATTESTATION":
                targetKey = key.toLowerCase();
                break;
            default:
                return;
        }
        String targetValue = String.valueOf(value);
        if (targetValue.isEmpty() || targetValue.equals(null) || targetValue == null) {
            targetValue = "";
        }
        SystemProperties.set(
                TextUtils.formatSimple("persist.sys.pihooks.%s", targetKey), targetValue);
    }

    private void restartApps() {
        if (mPm == null) {
            mPm = getActivity().getPackageManager();
        }
        if (mClearCacheObserver == null) {
            mClearCacheObserver = new ClearCacheObserver();
        }
        mPm.deleteApplicationCacheFiles("com.google.android.gms", mClearCacheObserver);
        mPm.deleteApplicationCacheFiles("com.android.vending", mClearCacheObserver);
    }

    private class ClearCacheObserver extends IPackageDataObserver.Stub {
        public void onRemoveCompleted(final String packageName, final boolean succeeded) {
            CustomUtils.restartApp(packageName, getActivity());
        }
    }

    private void initInfoPreferences(String prop, boolean attest, HashMap<String, Preference> map) {
        final Preference pref =
                (Preference) findPreference(attest ? prop + "_for_attestation" : prop);
        map.put(prop, pref);
    }

    private boolean setSummaryIfNotEmpty(
            String prop, boolean attest, HashMap<String, Preference> map) {
        final Preference pref = (Preference) map.get(prop);
        if (pref == null) return false;
        String ret = PiHookProperties.get(prop, "", attest);
        if (ret.isEmpty()) {
            if (attest) {
                mAttestation.removePreference(pref);
            } else {
                mInfo.removePreference(pref);
            }
            return false;
        }
        pref.setSummary(ret);
        return true;
    }

    private void updateSummary(boolean init) {
        ArrayList<String> infoPrefs = new ArrayList<String>();
        infoPrefs.add("device");
        infoPrefs.add("fingerprint");
        infoPrefs.add("product");
        infoPrefs.add("model");
        infoPrefs.add("brand");
        infoPrefs.add("security_patch");
        infoPrefs.add("manufacturer");
        infoPrefs.add("board");
        infoPrefs.add("hardware");
        infoPrefs.add("device_initial_sdk_int");
        infoPrefs.add("release");
        infoPrefs.add("id");
        infoPrefs.add("incremental");
        infoPrefs.add("type");
        infoPrefs.add("tags");
        if (init) {
            for (String i : infoPrefs) {
                initInfoPreferences(i, false, mInfoPreferences);
            }
        }
        for (String i : infoPrefs) {
            setSummaryIfNotEmpty(i, false, mInfoPreferences);
        }
        // *_for_attestation
        infoPrefs.clear();
        infoPrefs.add("device");
        infoPrefs.add("product");
        infoPrefs.add("model");
        infoPrefs.add("brand");
        infoPrefs.add("manufacturer");
        if (init) {
            for (String i : infoPrefs) {
                initInfoPreferences(i, true, mAttestationPreferences);
            }
        }
        for (String i : infoPrefs) {
            setSummaryIfNotEmpty(i, true, mAttestationPreferences);
        }
    }
}
